
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculate Hydraulic Potential from Surface and Bed elevation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rho_i = 916;              %density of ice
rho_w = 1000;              % density of water

g_ac = 9.8;                % gravitational accelerationn

% cd 'E:\Routing\routing script\Hydraulic_potential\updated_hyd_dem_250m'
% load 'DEM_bed_hyd_250.mat'
% load 'DEM_surf_hyd_250.mat'
% load 'mask_hyd_new_EN.mat'

load('hy_surf_all.mat')

% mask_hyd(mask_hyd<7) = nan;
% mask_hyd(mask_hyd==97)= nan;
% mask_hyd(isnan(mask_hyd))= 0;
% mask_hyd(mask_hyd>90)=0;
% DEM_bed_250(mask_hyd==0)= nan;
% DEM_surf_250(mask_hyd==0)= nan;
% 
% 
% Ice_thickness = DEM_surf_250 - DEM_bed_250;
% % Ice_thickness(mask_hyd>90)= 0;    % To remove the non-glaciated part.
% % Ice_thickness(mask_hyd==9) = Ice_thickness(mask_hyd==9)*0.1;
% Surface_elevation = DEM_surf_250;
% Bed_elevation = DEM_bed_250;

%% Calculate Hydraulic Potential

hy_pot = factor_k*(rho_i).* g_ac.*Ice_thickness + rho_w.* g_ac .*(Surface_elevation - Ice_thickness);

% hd_pres = factor_k * (rho_i./rho_w).* Ice_thickness;
% hd_elev = Bed_elevation; 

hy_head = factor_k*(rho_i./rho_w).*Ice_thickness + (Surface_elevation - Ice_thickness);

