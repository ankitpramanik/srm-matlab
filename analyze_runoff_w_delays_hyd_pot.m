
% clearvars;
delay_type = 1;
period = 0.125;  % in day

[SV_DEM,NX,NY,SV_DEM_Go,x_dem,y_dem,mask,mask_fjord,factor_k] = get_HYD_DEM_kfj(0);

SV_DEM_iis = find(SV_DEM>0);
l_SV_DEM = length(SV_DEM_iis);

%% Topographic Drainage

FD = FLOWobj(SV_DEM_Go,'preprocess','carve');
% Flow_accumulation = flowacc(FD);
FA = flowacc(FD);
[DB,Outlets] = drainagebasins(FD);
DB.Z = single(DB.Z);
DB.Z(DB.Z==0) = nan;
[x_outlet,y_outlet] = ind2coord(SV_DEM_Go,Outlets);

%% Flow accumulation at outlet points
flow_am = FA.Z(Outlets);
[As1,ind_out] = sort(flow_am,'descend');

%% Streams,Flow direction
Stream = STREAMobj(FD,FA>5);

%% Set up the transfer function
Flow_Distance = flowdistance(FD);
Avg_Q_Wavespeed = 0.6;      %m/s
Delay = Flow_Distance.Z/(Avg_Q_Wavespeed*3600*24*period);
Delay_vector = Delay(SV_DEM_iis);

Delay_vector(Delay_vector==0)= 125/(Avg_Q_Wavespeed *3600*24*period);

% Initialize the transfer function
TF = zeros(20,l_SV_DEM);
if delay_type== 1  % simple delay
    for ii = 1:l_SV_DEM
         TF(1+round(Delay_vector(ii)),ii)=1;
%          TF(1+round(Delay_vector(ii)),ii)=1;
          
    end 
else
    k = 1;
    for ii = 1:l_SV_DEM % decaying exponential
        TF(1:20,ii) = (exp(-[1:20]/(k*Delay_vector(ii)))/sum(exp(-[1:20]/(k*Delay_vector(ii)))))';
%         TF(1:20,ii) = (exp(-[1:20]*Delay_vector(ii))/sum(exp(-[1:20]*Delay_vector(ii))));
%         TF(1:20,ii) = (exp(-[1:20]/(k*Delay_vector(ii)^2))/sum(exp(-[1:20]/(k*Delay_vector(ii)^2))));
%         TF(1:20,ii) = (([1:20]/(Delay_vector(ii)))/sum([1:20]/(Delay_vector(ii))));
% 
    end 
end
% TF = flipud(TF);

%% Initialize variables
cd E:\Routing\runoff_data\kfj_basin_3hr_NA_17
% load runinfo.mat
time_start_run = '1-Jan-2015';               % start date calculation mean
time_start = '1-Jan-2017';
time_end = '16-Nov-2017 0:00';    %time.te;                 % end date calculation mean
tt = datenum(time_start):period:datenum(time_end);
Acc_temp = GRIDobj(SV_DEM_Go);
    
T = floor((datenum(time_end) - datenum(time_start))/period)+1;
shift = floor((datenum(time_start) - datenum(time_start_run))/period);

fid = fopen('roff_hyd_250_NA_17.gdat','rb');   % Be aware of unit of runoff!! 
frewind(fid)

kk = 0;
for t=1:T
    kk = kk+1;
%     fseek(fid,(t-datenum(time_start_run))*4*NX*NY,'bof');
    fseek(fid,(shift+t-1)*4*NX*NY,'bof');
    A = fread(fid,[NX NY],'real*4','l');
    A = A*250*250/(3600*24*period);            % converting from m.w.e/day to m^3/s
    A(mask_fjord==100)= nan;
    
   if max(A(:))==0,
        Runoff = zeros(size(SV_DEM));   
    else
%         temp = flipud(A);
        temp = A;
        temp(temp<0) = 0;
        Runoff = temp.*(SV_DEM>0);
   end 
   
   roff = GRIDobj(SV_DEM_Go);
   roff.Z = Runoff;
   
   Acc_runoff = flowacc(FD,roff);
   Discharge_no_delay(kk,:) = Acc_runoff.Z(Outlets);
   
   Runoff_year(kk,:)= Runoff(SV_DEM_iis);
   
   %% Do Convolution at the end of the date
   if t == T   %datenum(time_end)
       for ii=1:l_SV_DEM,
           conv_temp=conv(Runoff_year(:,ii),TF(:,ii),'full');
           Runoff_year2(:,ii) = conv_temp(1:size(Runoff_year(:,ii),1));
       end
       
       for jj = 1:size(Runoff_year2,1)
            Acc_temp.Z(SV_DEM_iis) = Runoff_year2(jj,:);
            Acc_runoff = flowacc(FD,Acc_temp);
            Discharge_w_delay(jj,:) = Acc_runoff.Z(Outlets);
       end 
   end            
   t
end 
fclose(fid);
    
