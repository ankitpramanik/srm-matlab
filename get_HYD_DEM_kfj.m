
function [SV_DEM,NX,NY,SV_DEM_Go,x_dem,y_dem,mask,mask_fjord,factor_k] = get_HYD_DEM_kfj(LORES)

factor_k = 0.9;
hyd_pot_cal

hy_head(mask_fjord==100)= nan;

cd 'E:\Routing\DEMs'
DEM = GRIDobj('dem_new_output_250m_new.tif');
DEM.size = [225 212];     % 197 173
DEM.cellsize = 250;
SV_DEM_Go = GRIDobj(DEM);
SV_DEM_Go.Z = hy_head;
SV_DEM_Go.Z(mask==0)= nan;
[SV_DEM,x_dem,y_dem] = GRIDobj2mat(SV_DEM_Go);
[x_dem,y_dem] = meshgrid(x_dem,y_dem);
SV_DEM(SV_DEM<=0) = NaN;

% load 'E:\Routing\runoff_data\kfj_basin\grid_250_glacier_land.mat'
% load 'E:\Routing\runoff_data\kfj_basin\runinfo.mat'

NX = 225;
NY = 212;

end 
